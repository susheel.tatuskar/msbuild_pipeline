﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LoginForm_Database_.Controllers
{
    public class HOMEController : Controller
    {
        // GET: HOME
        public ActionResult Index()
        {
            if (Session["Message"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
    }
}