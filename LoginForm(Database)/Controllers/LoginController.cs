﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using LoginForm_Database_.Models;

namespace LoginForm_Database_.Controllers
{
    public class LoginController : Controller
    {
        Entities db = new Entities();
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(LoginTable_1 s)
        {
            if (ModelState.IsValid == true)
            {
                var credentials = db.LoginTable_1.Where(model => model.UserName == s.UserName && model.Password == s.Password).FirstOrDefault();
                if (credentials == null)
                {
                    TempData["message"] = "<script>alert('username or password is incorrect')</script>";


                    return View();
                }
                else
                {
                    Session["Message"] = s.UserName;
                    return RedirectToAction("Index", "Home");
                }


            }
            return View();
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Login");

        }
        public ActionResult Reset()
        {
            ModelState.Clear();
            return RedirectToAction("Index", "Login");

        }
    }
}